const express = require('express');
const router = express.Router();
const app = express();
const mongoose = require('mongoose');
const expressEjsLayout = require('express-ejs-layouts')
const session = require('express-session');
const flash = require('connect-flash');
const passport = require('passport');
require("./config/passport")(passport)

//mongoose
mongoose.connect('mongodb://admin:H4ppyb0x-P0rt4l@localhost:27017/happybox-portal',{useNewUrlParser: true, useUnifiedTopology : true})

//mongoose.connect('mongodb://localhost/test',{useNewUrlParser: true, useUnifiedTopology : true})
.then(() => console.log('connected,,'))
.catch((err)=> console.log(err));
//EJS
app.set('view engine','ejs');
app.use(expressEjsLayout);
app.use(express.static(__dirname + '/public/'));
//BodyParser
app.use(express.urlencoded({extended : false}));
//express session
app.use(session({
  secret : 'secret',
  resave : true,
  saveUninitialized : true
 }));
  app.use(passport.initialize());
  app.use(passport.session());
 //use flash
 app.use(flash());
 app.use((req,res,next)=> {
   res.locals.success_msg = req.flash('success_msg');
   res.locals.error_msg = req.flash('error_msg');
   res.locals.error  = req.flash('error');
 next();
 })
//Routes
app.use('/',require('./routes/index'));
app.use('/users',require('./routes/users'));
app.use('/index',require('./routes/index'));
app.use('/orders', require('./routes/orders'));

var host = 'api.happyboxstore.com';
app.listen(5050, host , function(err) {
  if (err) return console.log(err);
  console.log("Listening at http://%s:%s", host , 5050);
}); 
//app.listen(3000); 