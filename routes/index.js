const express = require('express');
const router  = express.Router();
//login page
router.get('/', (req,res)=>{
    res.render('welcome');
})
//register page
router.get('/register', (req,res)=>{
    res.render('register');
})

router.get('/dashboard',(req,res)=>{
  if(req.user != undefined)
    {
    res.render('dashboard',{
      user: req.user
      });
    }
    else
    { res.render('dashboard',{
      user: ''
      });
    }
  })

  router.get('/login',(req,res)=>{
    res.render('login',{
      });
    })

  router.get('/error',(req,res)=>{
    res.render('error',{
      user: req.user
      });
    })

module.exports = router; 