var express = require('express');
var router = express.Router();

// Require controller modules.
var orderController = require('../controllers/OrderController');


/// BOOK ROUTES ///

// GET catalog home page.
router.get('/orders', orderController.orders_get);