const express = require('express');
const router  = express.Router();
OrderController = require('../controllers/OrderController');

//login handle
router.get('/orders',(req,res)=>{
    res.render('orders',
    {
        user: req.user
    });
})

router.get('/orderDetails',(req,res)=>{
    res.render('orderDetails',
    {
        //user: req.user
    });
})

router.get('/orderReport',(req,res)=>{
    res.render('orderReport',
    {
        //user: req.user
    });
})

router.get('/GetOrders/:orderId', OrderController.getSpecificOrder);

router.get('/GetOrders/:from/:to', OrderController.getFilteredOrders);

router.get('/GetReport/:from/:to', OrderController.getOrdersReport);

router.get('/GetProductSales/:from/:to', OrderController.getFilteredReport);

router.get('/GetInventory', OrderController.getInventory);
    
router.get('/GetOrders', OrderController.getOrders); 

router.get('/GetComments/:orderId', OrderController.getComments); 

router.post('/postOrder', OrderController.postOrder); 

router.post('/postFullfilledBox', OrderController.postFullfilledBox); 

router.post('/postWrittenCard', OrderController.postWrittenCard); 
 //return router
module.exports  = router;

