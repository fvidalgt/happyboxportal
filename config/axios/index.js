// import axios from 'axios'
// import axiosRetry from 'axios-retry';
const axios = require('axios').default;
const axios_retry = require('axios-retry');


exports.api = function(config) {
// export const api = ({ config }) => {
	const { APP_URL, API_SECRET } = config

	const client =  axios.create({
		baseURL: `${APP_URL}/admin/api/`,
		headers: {
			'Content-Type': 'application/json',
			'X-Shopify-Access-Token': API_SECRET,
		},
	})
	axios_retry(client, { retries: 5, retryDelay: axiosRetry.exponentialDelay});

	return client;
}

exports.api_v = function() {
	//const { APP_URL, API_VERSION, API_SECRET } = config

	const client = axios.create({
		baseURL: `https://happy-box-store.myshopify.com/admin/api/2020-04/`,
		headers: {
			'Content-Type': 'application/json',
			'X-Shopify-Access-Token': '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',
		},
	})
	axios_retry(client, { retries: 5, retryDelay: axios_retry.exponentialDelay});

	return client;
}

exports.api_inventory = function() {
	//const { APP_URL, API_VERSION, API_SECRET } = config

	const client = axios.create({
		baseURL: `https://happy-box-store.myshopify.com/admin/locations/`,
		headers: {
			'Content-Type': 'application/json',
			'X-Shopify-Access-Token': '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',
		},
	})
	axios_retry(client, { retries: 5, retryDelay: axios_retry.exponentialDelay});

	return client;
}

exports.app = function(config) {
	const { APP_URL, API_SECRET } = config

	return axios.create({
		baseURL: `${APP_URL}/`,
		headers: {
			'Content-Type': 'application/json',
			'X-Shopify-Access-Token': API_SECRET,
		},
	})
}
