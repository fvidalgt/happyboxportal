import dotenv from 'dotenv'
import axios from 'axios'

import dev from './development.env'
import pro from './production.env'
import qa from './qa.env'

dotenv.config()

const {
	NODE_ENV,
	HTTPS,
	CERTBOT,
	MASS_ERASURE,
	SSL_URL_PRIVKEY,
	SSL_URL_CERT,
	SSL_URL_CHAIN,
} = process.env

let currentEnv = dev

switch (NODE_ENV) {
	case 'production':
		currentEnv = pro
		break
	case 'qa':
		currentEnv = qa
		break
}

currentEnv.HTTPS = JSON.parse(HTTPS.toLowerCase())
currentEnv.CERTBOT = JSON.parse(CERTBOT.toLowerCase())
currentEnv.MASS_ERASURE = JSON.parse(MASS_ERASURE.toLowerCase())
currentEnv.SSL_URL_PRIVKEY = SSL_URL_PRIVKEY
currentEnv.SSL_URL_CERT = SSL_URL_CERT

// console.log(NODE_ENV, currentEnv)

export default currentEnv
