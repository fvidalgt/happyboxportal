const mongoose = require('mongoose');
const UserSchema  = new mongoose.Schema({
  orderId :{
      type  : String,
      required : true
  } ,
  userName :{
    type  : String,
    required : true
} ,
comment :{
    type  : String,
    required : true
} ,
cardWritten :{
    type  : Boolean,
    required : true,
    default: false
} ,
boxFullfilled :{
    type  : Boolean,
    required : true,
    default: false
} ,
date :{
    type : Date,
    default : Date.now
}
});
const Order= mongoose.model('Order',UserSchema);

module.exports = Order;