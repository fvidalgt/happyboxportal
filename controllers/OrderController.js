// export default class OrderController {
// 	constructor({ config, OrderService }) {
// 		this._config = config
// 		this.orderService = OrderService
// 	}
//orderService = require('../services/OrderService');
const Order = require("../models/order.js");
const User = require("../models/user.js");
_api = require('../config/axios/index');

exports.postOrder= async function(req, res) {
	if(req.user != undefined)
	{
		const updatedOrder = new Order({ orderId: req.body.orderId, userName: req.user.name, comment : req.body.comment, date: new Date() });
		updatedOrder.save(function (err) {
			if (err)
			{	
				console.error(err);
				return res.status(500);
			} 
		});
		console.log(updatedOrder);
		return res.status(200).send('Ok');
	}
	else
	{
		return res.status(500).send('Error');
	}	
}

exports.postFullfilledBox= async function(req, res) {
	if(req.user != undefined)
	{
		let isFullfilled = "Box Fullfilled: " + req.body.isFullfilled;
		const newOrder = new Order({ orderId: req.body.orderId, userName: req.user.name, boxFullfilled:req.body.isFullfilled, comment : isFullfilled, date: new Date() });
					newOrder.save(function (err) {
						if (err)
						{	
							console.error(err);
							return res.status(500);
						} 
					});
		Order.updateMany({orderId: req.body.orderId},  
			{boxFullfilled : req.body.isFullfilled}, function (err, orders) { 
			if (err){ 
				console.log(err) 
			} 
			else{ 
				console.log("Updated orders : ", orders); 
			} 
		});	
		return res.status(200).send('Ok');
	}
	else{
		return res.status(500).send('Error');
	}
}

exports.postWrittenCard= async function(req, res) {
	let isCardWritten = "Card Written: " + req.body.isCardWritten;
	console.log(req.user);
	if(req.user != undefined)
	{
	const newOrder = new Order({ orderId: req.body.orderId, userName: req.user.name, cardWritten: req.body.isCardWritten, comment : isCardWritten, date: new Date() });
				newOrder.save(function (err) {
					if (err)
					{	
						console.error(err);
						return res.status(500);
					} 
				  });
	Order.updateMany({orderId: req.body.orderId},  
		{cardWritten: req.body.isCardWritten}, function (err, orders) { 
		if (err){ 
			console.log(err) 
		} 
		else{
			console.log("Updated orders : ", orders); 
		} 
	});
	return res.status(200).send('Ok');
	}
	else{
		return res.status(500).send('Error');
	}
}

exports.getComments= async function(req, res) {
	try {
		
		  return res.status(200);  
    } catch (err) {
        console.log(err)
        res.status(500)
    }
}

exports.getInventory= async function(req, res) {
	let finishProducts = false;
	let finishInventory = false;
	let fromId = 0;
	let InventoryId = 0;
	let updatedInventory = [];
	let inventoryData = []
	let products = [];
	const newProducts = [];
	const inventoryList = [];
		while (!finishInventory) {
		var inventory = await getInventory(InventoryId).catch((err) => {
			return res.status(500).json(err.response)
		})

		if (inventory.data.inventory_levels.length > 0 && InventoryId < 39112037236901) {

			for (const inventoryItem of inventory.data.inventory_levels) {
		
				inventoryList.push(inventoryItem)
			 }
			 InventoryId = inventory.data.inventory_levels[inventory.data.inventory_levels.length - 1].inventory_item_id;
			 
			}else {
				console.log('finish');
				console.log(inventoryList.length);
				finishInventory = true;
			}
		}

		while (!finishProducts) {
		 products = await getProducts(fromId,req.params.from, req.params.to).catch((err) => {
			return res.status(500).json(err.response)
		})

		if (products.data.products.length > 0 && fromId < 5957072611493) {
			for (const product of products.data.products) {
				if(product.product_type != 'Curated Boxes' && product.product_type != '' && product.product_type != 'Fee' && product.product_type != 'bundle')
				newProducts.push(product);
			}
			fromId = products.data.products[products.data.products.length - 1].id;
			} else {
				finishProducts = true;
			}
		}
		
		if (newProducts.length > 0) {
			for (const product of newProducts) {
				for (const key of inventoryList) {

						if(product.variants[0].inventory_item_id == key.inventory_item_id)
						{					
							key.ProductTitle = product.title;
							key.ProductPrice = product.variants[0].price;
							updatedInventory.push(key);
						}
					 }
					 inventoryList.push(updatedInventory);
		  	}
		}
		var json = updatedInventory
		var fields = Object.keys(json[0])
		var replacer = function(key, value) { return value === null ? '' : value } 
		var csv = json.map(function(row){
		return fields.map(function(fieldName){
			return JSON.stringify(row[fieldName], replacer)
		}).join(',')
		})
		csv.unshift(fields.join(',')) // add header column
		csv = csv.join('\r\n');
		   res.setHeader('Content-disposition', 'attachment; filename=inventory-' + new Date().toJSON().slice(0,10).replace(/-/g,'/')+ '.csv');
		   res.set('Content-Type', 'text/csv');
		   res.redirect('back');
		   return res.status(200).send(csv);
}

exports.getSpecificOrder= async function(req, res) {
	const products = [];
	const productImages = [];
	var descriptionList = [];
	var product = {};
	var productDescription = [];
	var cardDescription = [];
	var productComments = [];
	let order = await getSpecificOrder(req.params.orderId).catch((err) => {
		return res.status(500).json(err.response)
	})
	
	var properties = order.data.orders[0].line_items[0].properties;
	for (const key in properties) {
		if(Object.values(properties[key])[0].includes("Item_") || Object.values(properties[key])[0] == "Card" || Object.values(properties[key])[0] == "Box" || Object.values(properties[key])[0] == "From" || Object.values(properties[key])[0] == "Message")
		{
			if(Object.values(properties[key])[0].includes("Item_"))
			{
			product = Object.values(properties[key])[1].substring(Object.values(properties[key])[1].indexOf("x ") + 2);
			if(product)
			
			product = product.replace("&", "%26");
			product = product.replace("+", "%2B");
			var images = await getProductsByTitle(product).catch((err) => {
				return res.status(500).json(err.response)
			})
					try{
						productDescription = {"description": Object.values(properties[key])[1], "imageUrl": images.data.products[0].image.src };
				
					}
					catch(e)
					{
						productDescription = {"description": Object.values(properties[key])[1], "imageUrl": "https://cdn.shopify.com/s/files/1/1363/8433/files/Happy_Box_Logo_070b8c58-cf2b-4bf0-88ca-e4bc660eb47a.jpg?v=1604334974" }
					}	
			}

				if(Object.values(properties[key])[0] == "Message")
				{
					cardDescription.push({"Message": Object.values(properties[key])[1] })
				}

				 if(Object.values(properties[key])[0] == "From")
				{
					cardDescription.push({"From": Object.values(properties[key])[1] })
				}

				if(Object.values(properties[key])[0] == "Card" || Object.values(properties[key])[0] == "Box")
				{
					product = Object.values(properties[key])[1];
					var images = await getProductsByTitle(product).catch((err) => {
						return res.status(500).json(err.response)
					})
					try{
						productDescription = {"description": Object.values(properties[key])[1], "imageUrl": images.data.products[0].image.src };
						
					}
					catch(e)
					{
						productDescription = {"description": Object.values(properties[key])[1], "imageUrl": "https://cdn.shopify.com/s/files/1/1363/8433/files/Happy_Box_Logo_070b8c58-cf2b-4bf0-88ca-e4bc660eb47a.jpg?v=1604334974" }
					}
					}
			
			descriptionList.push(productDescription);				
		}				
	}
	order.data.orders.productData = descriptionList;
	var OrderComments = await Order.find({ orderId: req.params.orderId }).exec();
	for(comment of OrderComments)
	{
		var moment = require('moment');
		var fomatted_date = moment(comment.date).format('YYYY-MM-DD, h:mm:ss a');
		productComments.push({'comment' :comment.comment, 'date' :fomatted_date, 'createdBy' :comment.userName, 'cardWritten' :comment.cardWritten, 'boxFullfilled' :comment.boxFullfilled});
	}

	res.render('orderDetails',{'orderDetails' : order.data.orders, 'orderComments' : productComments, 'card' : cardDescription} );
} 

	exports.getFilteredOrders= async function(req, res) {

		let finish = false;
		let fromId = 0;
		const newOrders = [];
		const items = [];

		while (!finish) {
			let orders = await getFilteredOrders(fromId,req.params.from, req.params.to).catch((err) => {
				return res.status(500).json(err.response)
            })
			if (orders.data.orders.length > 0) {
				newOrders.push(...orders.data.orders);
				fromId = orders.data.orders[orders.data.orders.length - 1].id;
			} else {
				finish = true;
			}
		}
		res.render('orderList',{'ordersList' : newOrders} );
	}

	exports.getOrdersReport= async function(req, res) {

		let finish = false;
		let fromId = 0;
		let listOrders = [];
		const listProducts = [{date : req.params.from}];
		
			let products = await getFilteredReports(fromId,req.params.from, req.params.to).catch((err) => {
				return res.status(500).json(err.response)
			})
			if (products.data.products.length > 0) {
				for (const product of products.data.products) {
					if(product.product_type != 'Curated Boxes' && product.product_type != '' && product.product_type != 'bundle')
						listProducts.push(product);
				}
				fromProductId = products.data.products[products.data.products.length - 1].id;
			} 
			
		while (!finish) {
			let orders = await getOrders(fromId,req.params.from, req.params.to).catch((err) => {
				return res.status(500).json(err.response)
			})
			if (orders.data.orders.length > 0) {
				for(var order of orders.data.orders)
				{
					if (order.status === 'cancelled') {
						continue;
					}
					else{
						var properties = order.line_items[0].properties;
						let cont = 0;							
							for (const key in properties) {

								var newOrder = Object.assign({}, order);
								if(Object.values(properties[key])[0].includes("Item_") || Object.values(properties[key])[0] == "Card" || Object.values(properties[key])[0] == "Box")
								{	
									if(Object.values(properties[key])[0].includes("Item_"))
										{
											var productName = Object.values(properties[key])[1].substring(Object.values(properties[key])[1].indexOf("x ") + 2);										
											var productQuantity = Object.values(properties[key])[1].toString().split('x ');
											// var productInfo = await getProductsByTitle(productName).catch((err) => {
											// 	return res.status(500).json(err.response)
											// })
											newOrder.items = productName;
											newOrder.quantity = productQuantity[0];
											newOrder.type = "Gift";
											newOrder.total_price_usd = null;
											// try
											// 	{
											// 	newOrder.price = productInfo.data.products[0].variants[0].price;
											// 	newOrder.inventory_quantity = productInfo.data.products[0].variants[0].inventory_quantity;
											// 	newOrder.grams = productInfo.data.products[0].variants[0].grams;
											// 	newOrder.weight = productInfo.data.products[0].variants[0].weight;
											// 	}
											// catch(e)
											// 	{
											// 		//console.log(e);
											// 	}
										}
										else
										{
											if(Object.values(properties[key])[0].toString() == "Box")
											{
												newOrder.type = "Box";
											}
											else
											{
												newOrder.type = "Card";
												newOrder.total_price_usd = null;
											}
											var quantity = Object.values(properties[key])[1];
											var productQuantity = Object.values(properties[key])[1].toString();
											newOrder.items = quantity;
											newOrder.quantity = 1;
										}																						
											newOrder.line_items = null;
											newOrder.title = order.line_items[0].title;
											newOrder.sku = order.line_items[0].sku;
											newOrder.vendor = order.line_items[0].vendor;
											newOrder.fulfillment_service = order.line_items[0].fulfillment_service;
											newOrder.requires_shipping = order.line_items[0].requires_shipping
											newOrder.taxable = order.line_items[0].taxable;
											newOrder.gift_card = order.line_items[0].gift_card;
											newOrder.variant_inventory_management = order.line_items[0].variant_inventory_management;
											newOrder.variant_id = order.line_items[0].variant_id;
											listOrders.push(newOrder);						
								}
							}
					}		
					items = [];
					countItems = [];
			}
				console.log(fromId);
				fromId = orders.data.orders[orders.data.orders.length - 1].id;
			} else {			
				console.log(listOrders.length);	
				finish = true;
			}
		}
		
		console.log(listOrders[0]);
		res.setHeader('Content-disposition', 'attachment; filename=report-' + req.params.from + '-' + req.params.to + '.csv');
		   res.set('Content-Type', 'text/csv');
		var json = listOrders
		var fields = Object.keys(json[0])
		var replacer = function(key, value) { return value === null ? '' : value } 
		var csv = json.map(function(row){
		return fields.map(function(fieldName){
			return JSON.stringify(row[fieldName], replacer)
		}).join(',')
		})
		csv.unshift(fields.join(',')) // add header column
		csv = csv.join('\r\n');
		   return res.status(200).send(csv);
	}

	exports.getFilteredReport= async function(req, res) {

		let finish = false;
		let fromId = 0;
		let fromProductId = 0;
		const listOrders = [];
		const listProducts = [];
		const items = [];

			let products = await getFilteredReports(fromId,req.params.from, req.params.to).catch((err) => {
				return res.status(500).json(err.response)
			})
			//console.log(orders.data.products.length);
			if (products.data.products.length > 0) {
				for (const product of products.data.products) {
					if(product.product_type != 'Curated Boxes' && product.product_type != '' && product.product_type != 'bundle')
					{						
						product.inventory_quantity = product.variants[0].inventory_quantity;
						product.gross_sales = product.inventory_quantity * product.variants[0].price;
						product.variants = '';
						listProducts.push(product);
					}
				}
				fromProductId = products.data.products[products.data.products.length - 1].id;
			} 
			
		while (!finish) {
			let orders = await getFilteredOrders(fromId,req.params.from, req.params.to).catch((err) => {
				return res.status(500).json(err.response)
			})
			if (orders.data.orders.length > 0) {
				for(var order of orders.data.orders)
				{
					if (order.status === 'cancelled') {
						continue;
					}
					else{
						listOrders.push(order);
					}				
			}
				fromId = orders.data.orders[orders.data.orders.length - 1].id;
			} else {			
				console.log(listOrders.length);	
				finish = true;
			}
		}
	
			for (const order of listOrders) {
			if(order.line_items !== undefined)
			{
				for (const lineItem of order.line_items) {
					var created = order.updated_at.substr(0, 10);
					var price = {vendor: order.line_items[0].vendor, orderNumber: order.order_number, sellDate: created, shipping: order.total_shipping_price_set.shop_money.amount, taxes: order.total_tax_set.shop_money.amount, totalPrice: order.total_price};
					var orderNumber = {orderNumber: order.order_number};
					created = {sellDate: created}
					//items.push(created)
					//items.push(price);
					//items.push(orderNumber);
					for (const property of lineItem.properties) {
					var values = '';
					try{
						values = property.value.toString().split('x ');	
						values.date = order.created_at;	
						
						if(values[0] < 4)
						//console.log(items);
						items.push(values[1], values.date);			
					}
					catch(e)
					{
						console.log(e);
					}				
				}
			 }
			}
		//}
	}
		var productsSold = count(items);
		for (const product of listProducts) {
			for (const key in productsSold) {
				if(product.title == key)
				product.QuantitySold = productsSold[key];
				
			}
		}
		
		res.setHeader('Content-disposition', 'attachment; filename=report-' + req.params.from + '-' + req.params.to + '.csv');
		   res.set('Content-Type', 'text/csv');
		   var json = listProducts
			var fields = Object.keys(json[0])
			var replacer = function(key, value) { return value === null ? '' : value } 
			var csv = json.map(function(row){
			return fields.map(function(fieldName){
				return JSON.stringify(row[fieldName], replacer)
			}).join(',')
			})
			csv.unshift(fields.join(',')) // add header column
			csv = csv.join('\r\n');
		   return res.status(200).send(csv);
	}

	exports.getOrders = async function(req, res) {

		let finish = false;
		let fromId = 0;
		const newOrders = [];
		const items = [];

		while (!finish) {
			let orders = await getAsyncOrders(fromId).catch((err) => {
				return res.status(500).json(err.response)
            })
			if (orders.data.orders.length > 0) {
				newOrders.push(...orders.data.orders);
				fromId = orders.data.orders[orders.data.orders.length - 1].id;
			} else {
				finish = true;
			}
		}


        res.render('orderList',{'ordersList' : newOrders} );
		//return res.status(200).json(newOrders)
    } 
    
    async function getAsyncOrders(fromId) {
		let url = 'orders.json?created_at_min=' + new Date().toJSON().slice(0,10).replace(/-/g,'/') + 'T00:00:00-00:00&created_at_max=' + new Date().toJSON().slice(0,10).replace(/-/g,'/') + 'T14:00:00-00:00&limit=250&status=any';
		if (fromId >= 0)
			url += '&since_id=' + fromId
		return await _api.api_v().get(url, {
			timeout: 20000
		})
	}

	async function getFilteredOrders(fromId, from, to) {
		let url = 'orders.json?created_at_min=' + from + 'T00:00:00-00:00&created_at_max=' + to + 'T24:00:00-00:00&limit=250&status=any';
		if (fromId >= 0)
			url += '&since_id=' + fromId
			console.log(url);
		return await _api.api_v().get(url, {
			timeout: 50000
		})
	}

	async function getOrders(fromId, from, to) {
		let url = 'orders.json?created_at_min=' + from + 'T00:00:00-00:00&created_at_max=' + to + 'T24:00:00-00:00&limit=250&status=any&fields=id,name,email,financial_status,paid_at,fulfillment_status,total_price_usd,line_items,created_at';
		if (fromId >= 0)
			url += '&since_id=' + fromId
			console.log(url);
		return await _api.api_v().get(url, {
			timeout: 50000
		})
	}

	async function getSpecificOrder(orderId) {
		let url = 'orders.json?name=' + orderId + '&status=any';
		return await _api.api_v().get(url, {
			timeout: 20000
		})
	}

	async function getFilteredReports(fromId, from, to) {
		let url = 'products.json?fields=title,vendor,product_type,variants&limit=250';
		if (fromId >= 0)
			url += '&since_id=' + fromId
		return await _api.api_v().get(url, {
			timeout: 50000
		})
	}

	async function getProducts(fromId) {
		
		let url = 'products.json?limit=250';
		if (fromId >= 0)
			url += '&since_id=' + fromId
		return await _api.api_v().get(url, {
			timeout: 50000
		})
	}

	async function getProductsByTitle(title) {
		let url = 'products.json?title=' + title;
		return await _api.api_v().get(url, {
			timeout: 20000
		})
	}

	async function getInventory(inventoryId) {
		let url = '24285249/inventory_levels.json?limit=250';
		if (inventoryId >= 0)
		url += '&since_id=' + inventoryId
		return await _api.api_inventory().get(url, {
			timeout: 50000
		})
	}

	function count(array) {
		
		var counts = {};
		array.forEach(function(x) { counts[x] = (counts[x] || 0)+1; });	
		return counts;
	}

	function ConvertToCSV(objArray) {
		
		var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
		var str = '';

		for (var i = 0; i < array.length; i++) {
			var line = '';
			for (var index in array[i]) {
				if (line != '') line += ','

				line += array[i][index];
			}

			str += line + '\r\n';
		}
		return str;
		
	}
	

    // }


